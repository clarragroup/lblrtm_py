# README #

### What is this repository for? ###

* This repository includes Python codes for running the Line By Line Radiative Transfer Model (LBLRTM).
* Version 1

### Setting up ###

* Before running this code, LBLRTM must be set up on the computer. LBLRTM can be downloaded from here: http://rtweb.aer.com/lblrtm_frame.html. After set up, it is useful to create an alias to the compiled run file called “Lblrtm_dbl”.

### Trying out a sample run ###

* To see an example of how this code may be used, try out the sample_run_SouthPole.py, which calls LBLRTM for atmospheric profiles from South Pole Station to compute the downwelling infrared radiance. 

* Before running sample_run_SouthPole.py, you will need to change: 
    * The directory prof_dir to where you put the files prof*.nc provided below
    * The directory lblrtm_dir to where you put the lblrtm executable
    * The variable run_lblrtm, to the command you use to call lblrtm from the terminal. 

### Running LBLRTM for your atmospheric profiles ###

To modify sample_run_SouthPole.py to run with your own atmospheric profiles, you will need 
to put the profile data in netcdf files in the same format as the prof*.nc files. The required format is described in sample_run_SouthPole.py. 


### Acknowledgement ###

This code was developed by Dr. Penny M. Rowe. Please acknowledge its use in work that leads to publication.
