"""
radtran_LBLRTM.py

Copyright 2019 by Penny M. Rowe and NorthWest Research Associates.
All rights reserved.
"""

from reduce_resolution import reduce_resolution as reduce_res
import numpy as np
from read_od_lblrtm import read_od_lblrtm as rdODprec
from plancknu import plancknu
import time
from scipy import interpolate

def radtran_LBLRTM(od_dir, nlyr, tzl, tave, dnu, bwn, ewn, N, n, zs, 
                   zangle, prec = 's'):
    """
     by Penny Rowe
    
     Jan. 2019
    
    
     Purpose:
       Read in LBLRTM optical depths and do radiative transfer in a similar
       way to LBLRTM. Return finite-resolution and monochromatic-resolution
       wavenumber and radiance.
    
    
     Inputs in function call:
     od_dir:   directory with LBLRTM optical depths
     nlyr:  number of layers for which there are ods
     tzl:   temperature at layer bottom
     tave:  "ave" temperature, as defined by LBLRTM
     dnu:   wavenumber spacing (determined by resolution)
     bwn:
     ewn:
     prec:    precision for rdODprec
    
    /Users/prowe/Git_repos/lblrtm_py/read_od_lblrtm.py
     Variables used in this code:
    
         nu_mono: nu at monochromatic resolution, from lblrtm
         od_mono: layer optical depth, at monochromatic resolution, from lblrtm
         transLm1: above for surface-to-layer-minus-1
    """
  
    # ... QC
    if dnu<=0:
        raise NameError('Laser effective wavenumber must be > 0, \
                         this is low resolution.')
  
  
    if od_dir[-1] != '/':
        od_dir += '/'
    
    # .. Inputs for doing radiative transfer
    AA = 0.278                 # Pade coefficient
  
    nuLm1_mono = 0
    
  
    if len(tave) == 1:
        tave = (tzl[:len(tzl)-1] + tzl[1:len(tzl)]) / 2
  
    # Viewing angle
    cos_angle = np.cos(np.deg2rad(zangle))
  
    # get transmittance, rad., from the bottom layer up
    first_layer = 1; transLm1_mono = 1
    for ilayer in range(nlyr):
  
        if ilayer+1<=9:
            fileinp = 'ODdeflt_00' + str(ilayer+1)
        elif ilayer+1<=99:
            fileinp = 'ODdeflt_0' + str(ilayer+1)
        else:
            fileinp = 'ODdeflt_' + str(ilayer+1)
        
        # start = time.time()
        nu_mono, od_mono = rdODprec(od_dir+fileinp, prec)
        # end = time.time()
        #print('time to load file '+str(ilayer)+' : '+str(end - start))


        if (not first_layer) and (len(nu_mono) != len(nuLm1_mono)):
            # interp old stuff up
            #transLm1_mono = np.interp(nu_mono, nuLm1_mono, transLm1_mono)
            spl = interpolate.splrep(nuLm1_mono, transLm1_mono)
            transLm1_mono = interpolate.splev(nu_mono, spl)
      
            if any(np.isnan(transLm1_mono)):
                raise NameError("NaNs found in transLm1_mono")
    
            rad_mono = np.interp(nu_mono, nuLm1_mono, rad_mono)
            #spl = interpolate.splrep(nuLm1_mono, rad_mono)
            #rad_mono = interpolate.splev(nu_mono, spl)
      
    
    
        # ... The transmittance up to the bottom of this layer (transLm1_mono)
        #     times the transmittance of this layer, at monochromatic resolution
        transL_mono = transLm1_mono * np.exp(-od_mono / cos_angle)  
        
        
    
        # ... Get Planck function of the layer in the same way as LBLRTM.
        #     BBA corresponds to the lower boundary temperature;  since
        #     ibnd2 corresponds to layer tops, the slower bound is ibnd2-1
        #     Similarly, BB corresponds to the average temperature, which
        #     is for index ibnd2-1 for layer top ibnd2
        XX = AA * od_mono / cos_angle
        BB = plancknu(nu_mono, tave[ilayer])
        BBA = plancknu(nu_mono, tzl[ilayer])
        BL = (  (BB + XX*BBA) / (1 + XX) )
            

        # ... The layer radiance that reaches the surface
        rad0_mono = -BL * (transL_mono - transLm1_mono)
        
        if first_layer:
            rad_mono = rad0_mono
            first_layer = 0
        else:
            rad_mono += rad0_mono
      
      
        # ... Set values for next loop through
        nuLm1_mono = nu_mono
        transLm1_mono = transL_mono

  
    rad_mono = 1e3 * rad_mono
  
      
    
    # ... Reduce resolution
    nu, rad = reduce_res(nu_mono, rad_mono, dnu, N, n)
    rad = np.real(rad)

    return(nu, rad, nu_mono, rad_mono)
  

