#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 29 14:48:32 2020

@author: prowe

Copyright 2020 by Penny M. Rowe and NorthWest Research Associates.
All rights reserved.

Purpose: Run LBLRTM for a specified atmospheric profile to get radiance

Some of the Inputs:
    prof_file: name of netcdf file with profile data
    v1: starting wavenumber, float, e.g. 300.  
    v2: ending wavenumber, float, e.g. 1400.
    zangle: viewing angle from zenith, e.g. 0. for downwelling
    
    prof_file:
        A netcdf file with the following variables:
            * Required variables *
            z: Model atmosphere boundary levels: "zm" in LBLRTM
               Units do not need to be specified, MUST be km
            P: Model atmosphere boundary pressures: "pm" in LBLRTM
               Units MUST be specified as 'mb'
            T: Model atmosphere boundary temperatures: "tm" in LBLRTM
               Units MUST be specified as 'K'
            
            * Optional: gases 1-16 and CFC concentrations *
            'h2o', 'co2', 'o3', 'n2o', 'co', 'ch4', 'o2', 'no',
            'so2', 'no2', 'nh3', 'hno3', 'oh', 'hf', 'hcl', 'hbr'
            'f11', 'f12', 'f113'
               For each, units must be specified as 'ppmv', 'gm_kg', or 'B'
               where B corresponds to one of LBLRTM jchar values.
            
            * Optional *
            model_extra: corresponds to "model" in LBLRTM, except that it is 
                         only applied to gases that are not specified, 
                         up to nmol
            nmol: Number of gases to include in LBLRTM calculation, where 
                  gases are in the order given above. If nmol is not included 
                  in the netcdf file, it is set based on the highest gas number
                  specified (e.g. if 'o3' is the last gas specified, nmol = 3)
                  If nmol is included in the netcdf file, the maximum of
                  the specified value and the highest gas number is used.
            
Notes:
    On running LBLRTM, it is normal to get the following messages for each run:
        Note: The following floating-point exceptions are signalling: 
            IEEE_UNDERFLOW_FLAG IEEE_DENORMAL
        STOP  LBLRTM EXIT 
        
        

"""


# .. Directories where needed code resides.
#    This needs to be set for the particular computer
import site
site.addsitedir("/Users/prowe/Git_repos/lblrtm_py/")


# .. Built-in modules
import matplotlib.pyplot as plt
from netCDF4 import Dataset

# .. My LBLRTM modules
import lblrtm_resources




# # # # # # # # # #      INPUT PARAMETERS     # # # # # # # # # # 
# .. Directories where output profile files and LBLRTM reside
#    and where output should be moved to
prof_dir = '/Users/prowe/Git_repos/lblrtm_py/sample_run_SouthPole/'  # profiles
lblrtm_dir = '/Users/prowe/Projects/radtran/Lblrtm/run1/'            # LBLRTM

# .. File names
prof_files = ['prof20010115_2038.nc',
              'prof20010215_0832.nc', 
              'prof20010815_2100.nc']

# .. Parameters
run_lblrtm = lblrtm_dir + 'Lblrtm_dbl'  # command for calling LBLRTM
v1 = 500                   # desired starting wavenumber
v2 = 1200                   # desired ending wavenumber
zangle = 0                 # zenith viewing angle: 0 => straight up
npts_lo = 16384            # Number of points in low-resolution interferogram
npts_hi = 2**22            # Number of points in high-resolution interferogram
dnu = 0.5                  # Desired wavenumber resolution
iprof = 0                  # Index to profile files that you want to run
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 



prof_file = prof_files[iprof]

# .. Set values in class instance "prof" based on file prof_file
profileForLBLRTM = lblrtm_resources.ProfileForLBLRTM(prof_dir + prof_file)

nu, rad, \
nu_mono, \
rad_mono = lblrtm_resources.get_rad_lblrtm(profileForLBLRTM, npts_lo, npts_hi, 
                                       dnu, lblrtm_dir, run_lblrtm, v1, v2, 
                                       zangle, n_padded=0)


# .. Also load in the profile file. We don't really need it, but the
#    format is a bit nicer than that of profileForLBLRTM
with Dataset(prof_file, 'r', format='NETCDF4_CLASSIC') as prof:

    # .. Plot the temperature, water vapor and radiance
    plt.figure(1); plt.clf()
    plt.subplot(position =[.1, .6, .39, .38])
    plt.plot(prof.variables['T'][:], prof.variables['z'][:])
    plt.ylabel('Altitude (' + prof['z'].units + ')')
    plt.xlabel('Temperature (' + prof['T'].units + ')')
    
    plt.subplot(position =[.58, .6, .39, .38])
    plt.plot(prof.variables['h2o'][:], prof.variables['z'][:])
    plt.ylabel('Altitude (' + prof['z'].units + ')')
    plt.xlabel('Water vapor (' + prof['h2o'].units + ')')
    
    plt.subplot(position =[.1,.1, .86, .37])
    plt.plot(nu_mono, rad_mono, label = 'radiance, monochrom. resolution')
    plt.plot(nu, rad, label = 'radiance, instrument resolution')
    plt.legend(); plt.xlim([500, 1200])
    plt.xlabel('wavenumber'); plt.ylabel('Radiance (mW / [m$^2$ sr cm$^{-1}$])')




